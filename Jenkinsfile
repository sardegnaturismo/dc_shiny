pipeline {
    agent { 
        label 'jnlp-docker' 
    }
    environment {
        SERVICE = 'shiny-dc'
        REGISTRY = '234226431818.dkr.ecr.eu-west-1.amazonaws.com/${SERVICE}'
        ENVIRONMENT = 'stage'
        MICROSERVICE_VERSION = '2018'
    }
    stages {
         stage('Build Docker Image') {
             steps {
                 sh "docker build -t ${SERVICE}:${MICROSERVICE_VERSION} ."
             }
         }
         stage('Tag Docker Image') {
             steps {
             sh "docker tag ${SERVICE}:${MICROSERVICE_VERSION} ${REGISTRY}:${MICROSERVICE_VERSION}"
             }
         }
         stage('Push Docker Image') {
             steps {
                 script {
                     def login = ecrLogin()
                     sh "${login}"
                 }
                 sh "docker push ${REGISTRY}:${MICROSERVICE_VERSION}"
             }
         }
        stage('Get config files') {
            steps {
                withAWSParameterStore(credentialsId: 'rastur-stage', naming: 'basename', path: '/stage/dashboard-cittadino-2018', recursive: true, regionName: 'eu-west-1') {
                    writeFile file: 'config.env', text: "$CONFIG"
                }
            }
        }
        stage('Get kustomize repository') {
            steps {
                checkout([$class: 'GitSCM', branches: [[name: '*/master']], doGenerateSubmoduleConfigurations: false, extensions: [], submoduleCfg: [], userRemoteConfigs: [[credentialsId: 'rchessa-bitbucket', url: 'https://bitbucket.org/sardegnaturismo/kustomize.git']]])
            }
        }
        stage('Run kustomize') {
            steps {
                // ADD configmap files
                sh "cp config.env ${SERVICE}-${MICROSERVICE_VERSION}/overlays/${ENVIRONMENT}/"
                sh "/usr/local/bin/kustomize build ${SERVICE}-${MICROSERVICE_VERSION}/overlays/${ENVIRONMENT} > ${SERVICE}-${MICROSERVICE_VERSION}.yaml"
            }
        }
        stage('Upload to S3 bucket') {
            steps {
                withAWS(credentials: 'ras-stage-kustomize') {
                    s3Upload (acl: 'Private', bucket: 'ras-stage-kustomize', cacheControl: '', file: "${SERVICE}-${MICROSERVICE_VERSION}.yaml", path: "${ENVIRONMENT}/${SERVICE}/${SERVICE}-${MICROSERVICE_VERSION}.yaml")
                }
            }
        }
        stage('Archive docker tag') {
            steps {
                writeFile file: 'imageTag', text: "IMAGE_TAG=${MICROSERVICE_VERSION}"
                archiveArtifacts artifacts: 'imageTag', fingerprint: true, onlyIfSuccessful: true
            }
        }
    }
}
